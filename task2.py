import sys
import random

def binary_search(arr, x):
	low = 0
	high = len(arr)-1
	mid = 0

	while low <= high:
		mid = (high+low)//2
		if arr[mid] < x:
			low = mid+1
		elif arr[mid] > x:
			high = mid+1
		else:
			return mid

	return -1


arr = sorted([random.randint(1, 1000) for _ in range (100)])
print ("Array", arr)

if len(sys.argv) > 1:
	x = int(sys.argv[1])
else:
	x = 0
	
result = binary_search(arr, x)

if result!= -1:
	print ("Got it. The index is", str (result))
else:
	print ("GGWP, try again")

