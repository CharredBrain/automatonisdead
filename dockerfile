FROM python:3.9-slim
WORKDIR /2
COPY task2.py .
CMD ["python", "task2.py"]
